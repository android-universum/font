@Font-Widget-Common
===============

This module groups the following modules into one **single group**:

- [Widget-Core](https://bitbucket.org/android-universum/font/src/main/library-widget-core)
- [Widget-Button](https://bitbucket.org/android-universum/font/src/main/library-widget-button)
- [Widget-Text](https://bitbucket.org/android-universum/font/src/main/library-widget-text)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Afont/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Afont/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:font-widget-common:${DESIRED_VERSION}@aar"

_depends on:_
[font-core](https://bitbucket.org/android-universum/font/src/main/library-core),
[font-widget-core](https://bitbucket.org/android-universum/font/src/main/library-widget-core)