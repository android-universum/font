/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import android.os.Build;

import org.junit.Test;
import org.robolectric.annotation.Config;

import universum.studios.android.font.widget.FontButton;
import universum.studios.android.test.AndroidTestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author Martin Albedinsky
 */
public final class FontButtonTest extends AndroidTestCase {

	private static final String FONT_PATH = "redressed.ttf";

	@Test public void testInstantiation() {
		// Act:
		new FontButton(context());
		new FontButton(context(), null);
		new FontButton(context(), null, android.R.attr.buttonStyle);
	}

	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testInstantiationViaLollipopConstructor() {
		// Act:
		new FontButton(context(), null, android.R.attr.buttonStyle, 0);
	}

	@Test public void testSetFont() {
		// Arrange:
		final FontApplier mockApplier = mock(FontApplier.class);
		final FontButton widget = new FontButton(context());
		widget.setFontApplier(mockApplier);
		final Font font = Font.create(FONT_PATH);
		// Act:
		widget.setFont(font);
		// Assert:
		verify(mockApplier).applyFont(widget, font);
	}

	@Test public void testSetFontPath() {
		// Arrange:
		final FontApplier mockApplier = mock(FontApplier.class);
		final FontButton widget = new FontButton(context());
		widget.setFontApplier(mockApplier);
		// Act:
		widget.setFont(FONT_PATH);
		// Assert:
		verify(mockApplier).applyFont(widget, FONT_PATH);
	}

	@Test public void testSetFontViaTextAppearance() {
		// Arrange:
		final FontApplier mockApplier = mock(FontApplier.class);
		final FontButton widget = new FontButton(context());
		widget.setFontApplier(mockApplier);
		// Act:
		widget.setTextAppearance(context(), 0);
		// Assert:
		verify(mockApplier).applyFont(widget, 0);
	}
}