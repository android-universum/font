Font-Widget-Selection
===============

This module contains **selection widgets** that implement `FontWidget` and thus allow usage of custom font.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Afont/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Afont/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:font-widget-selection:${DESIRED_VERSION}@aar"

_depends on:_
[font-core](https://bitbucket.org/android-universum/font/src/main/library-core),
[font-widget-core](https://bitbucket.org/android-universum/font/src/main/library-widget-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FontCheckBox](https://bitbucket.org/android-universum/font/src/main/library-widget-selection/src/main/java/universum/studios/android/font/widget/FontCheckBox.java)
- [FontRadioButton](https://bitbucket.org/android-universum/font/src/main/library-widget-selection/src/main/java/universum/studios/android/font/widget/FontRadioButton.java)