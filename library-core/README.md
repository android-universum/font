Font-Core
===============

This module contains core components and interfaces that are used to support usage of **custom font**
in an **Android** application.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Afont/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Afont/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:font-core:${DESIRED_VERSION}@aar"
    
## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Font](https://bitbucket.org/android-universum/font/src/main/library-core/src/main/java/universum/studios/android/font/Font.java)
- [FontWidget](https://bitbucket.org/android-universum/font/src/main/library-core/src/main/java/universum/studios/android/font/FontWidget.java)