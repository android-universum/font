/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import android.graphics.Typeface;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public final class FontApplierTest extends AndroidTestCase {

	private static final String FONT_PATH = "redressed.ttf";

	@Override public void afterTest() {
		super.afterTest();
		Font.clearCache();
	}

	@Test public void testApplyFontFromNullAttributeSetWithDefaultStyle() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.TextAppearance.WithFont",
				"style",
				context().getPackageName()
		);
		final FontWidget mockWidget = mock(FontWidget.class);
		when(mockWidget.getContext()).thenReturn(context());
		// Act:
		assertThat(FontApplier.DEFAULT.applyFont(mockWidget, null, 0, styleResId), is(true));
		// Assert:
		verify(mockWidget).setTypeface(any(Typeface.class));
	}

	@Test public void testApplyFontFromStyle() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.TextAppearance.WithFont",
				"style",
				context().getPackageName()
		);
		final FontWidget mockWidget = mock(FontWidget.class);
		when(mockWidget.getContext()).thenReturn(context());
		// Act:
		assertThat(FontApplier.DEFAULT.applyFont(mockWidget, styleResId), is(true));
		// Assert:
		verify(mockWidget).setTypeface(any(Typeface.class));
	}

	@Test public void testApplyFontFromStyleWithoutFontAttribute() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.TextAppearance.WithoutFont",
				"style",
				context().getPackageName()
		);
		final FontWidget mockWidget = mock(FontWidget.class);
		when(mockWidget.getContext()).thenReturn(context());
		// Act:
		assertThat(FontApplier.DEFAULT.applyFont(mockWidget, styleResId), is(false));
		// Assert:
		verify(mockWidget, times(0)).setTypeface(any(Typeface.class));
	}

	@Test public void testApplyFontFromPath() {
		// Arrange:
		final FontWidget mockWidget = mock(FontWidget.class);
		when(mockWidget.getContext()).thenReturn(context());
		// Act:
		assertThat(FontApplier.DEFAULT.applyFont(mockWidget, FONT_PATH), is(true));
		// Assert:
		verify(mockWidget).setTypeface(any(Typeface.class));
	}

	@Test public void testApplyFont() {
		// Arrange:
		final FontWidget mockWidget = mock(FontWidget.class);
		when(mockWidget.getContext()).thenReturn(context());
		final Font font = Font.create(FONT_PATH);
		// Act:
		assertThat(FontApplier.DEFAULT.applyFont(mockWidget, font), is(true));
		// Assert:
		verify(mockWidget).setTypeface(any(Typeface.class));
	}
}