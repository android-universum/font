/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import android.graphics.Typeface;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

/**
 * @author Martin Albedinsky
 */
public final class FontTest extends AndroidTestCase {

	private static final String FONT_PATH = "redressed.ttf";

	@Override public void afterTest() {
		super.afterTest();
		// Ensure that the Font class is always in the initial state.
		Font.clearCache();
	}

	@Test public void testCreateFromStyleWithPath() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.TextAppearance.WithFont",
				"style",
				context().getPackageName()
		);
		// Act:
		final Font font = Font.create(context(), styleResId);
		// Assert:
		assertThat(font, is(not(nullValue())));
		assertThat(font.getFilePath(), is(Font.FONT_DIR + FONT_PATH));
	}

	@Test public void testCreateFromStyleWithoutPath() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.TextAppearance.WithoutFont",
				"style",
				context().getPackageName()
		);
		// Act:
		final Font font = Font.create(context(), styleResId);
		// Assert:
		assertThat(font, is(nullValue()));
	}

	@Test public void testGetTypeface() {
		// Arrange:
		final Font font = new Font(Font.FONT_DIR + FONT_PATH);
		// Act:
		final Typeface typeface = font.getTypeface(context());
		// Assert:
		assertThat(typeface, is(notNullValue()));
		assertThat(typeface, is(font.getTypeface(context())));
	}
}