/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;

/**
 * Font is a simple wrapper class around graphics {@link Typeface} object. Each Font instance is
 * required to be created with a relative path to its corresponding .ttf font file that is placed
 * within <b>assets</b> directory. A new Font instance may be created directly via {@link #Font(String)}
 * constructor, however it is encouraged to create new instances of Font via one of {@link #create(String)},
 * {@link #create(Context, int)} or {@link #create(Context, AttributeSet, int, int)} factory methods,
 * because these methods ensure that already created fonts are reused across entire Android application
 * which definitely improves performance as creation of a single Typeface object via
 * {@link Typeface#createFromAsset(AssetManager, String)} is a costly operation.
 * <p>
 * This library supports only <b>.ttf</b> font files. Each font file is required to be placed within
 * assets directory of the corresponding application. A base path to font files may be specified via
 * {@link #setBasePath(String)} which will be used by the Font class when building full path to a
 * specific .ttf file whenever its {@link Typeface} needs to be created. When base path is used all
 * paths to .ttf files specified via {@link R.attr#uiFont uiFont} may be relative in association to
 * that base path. Relative path to a specific <b>true typeface font</b> file may or may not have
 * specified <b>.ttf</b> suffix. If the suffix is not specified, this class ads such suffix automatically.
 * <p>
 * Typeface object of a specific Font instance may be obtained via {@link #getTypeface(Context)}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class Font {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "Font";

	/**
	 * Suffix for original <b>true Typeface</b> fonts.
	 * <p>
	 * Constant value: <b>.ttf</b>
	 */
	public static final String TTF_SUFFIX = ".ttf";

	/**
	 * Sub-directory within an application assets folder where should be, by default, placed all
	 * custom font files.
	 * <p>
	 * However, this directory may contain custom sub-directories in order to group related fonts
	 * together.
	 * <p>
	 * Constant value: <b>font/</b>
	 *
	 * @see #setBasePath(String)
	 */
	public static final String FONT_DIR = "font" + File.separator;

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/**
	 * Matcher for <b>.ttf</b> file suffix.
	 */
	private static final Matcher TTF_SUFFIX_MATCHER = Pattern.compile("(.*)\\.ttf").matcher("");

	/**
	 * Log level controlling whether this class logs messages into console or not.
	 */
	private static int logLevel = Log.ASSERT;

	/**
	 * Base path at which are stored all .ttf files managed by this font class. This path is used to
	 * build a full path to a specific font whenever {@link #buildFullPath(String)} is called.
	 */
	private static String basePath = FONT_DIR;

	/**
	 * Cache containing already created fonts. The fonts are stored and mapped by theirs corresponding
	 * font path.
	 */
	private static final Map<String, Font> cache = new HashMap<>(5);

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Path to the .ttf file within an application assets folder which is represented by this font
	 * instance.
	 */
	private final String filePath;

	/**
	 * Typeface created by this font from the corresponding .ttf file.
	 */
	private Typeface typeFace;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of Font with the specified path that points to a .ttf file within assets
	 * folder.
	 * <p>
	 * Consider using {@link #create(String)} instead which may return already created and cached
	 * instance of {@code Font}.
	 * <p>
	 * <b>Note that empty or {@code null} paths are not allowed.</b>
	 * <p>
	 * <b>Only .ttf font files are supported!</b>
	 *
	 * @param filePath Relative path to the .ttf file (with or without .ttf suffix) placed within
	 *                 an application assets directory which will be represented by the newly created
	 *                 Font instance.
	 * @throws IllegalArgumentException If the given <var>filePath</var> is empty.
	 */
	public Font(@NonNull final String filePath) {
		if (TextUtils.isEmpty(filePath)) {
			throw new IllegalArgumentException("Font path cannot be empty.");
		}
		this.filePath = filePath;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Sets a logging level determining at which level should be visible messages logged by Font class
	 * into console via {@link Log} utility.
	 * <p>
	 * <b>Note that the current implementation of Font class logs only messages at
	 * {@link Log#DEBUG} level.</b>
	 * <p>
	 * Default value: {@link Log#ASSERT}
	 *
	 * @param level The desired log level.
	 */
	public static void setLogLevel(final int level) {
		logLevel = Math.max(Math.min(Log.ASSERT, level), Log.VERBOSE);
	}

	/**
	 * Logs the specified <var>message</var> via {@link Log#d(String, String)} if the logging level
	 * specified via {@link #setLogLevel(int)} allows it.
	 *
	 * @param message The message to be logged.
	 */
	private static void logDebugMessage(@NonNull final String message) {
		if (logLevel <= Log.DEBUG) Log.d(TAG, message);
	}

	/**
	 * Specifies a base path at which are stored font .ttf files that are to be created via
	 * {@link #create(String)} and cached by this Font class.
	 * <p>
	 * The specified path is used to build a full path to a specific font file whenever its representing
	 * {@link Typeface} needs to be created via {@link Typeface#createFromAsset(AssetManager, String)}.
	 * <p>
	 * Default value: {@link #FONT_DIR}
	 *
	 * @param basePath The desired base path within Android assets folder. May be {@code null} to not
	 *                 use any base path, when in such case the {@link R.attr#uiFont uiFont} attribute
	 *                 or path specified via {@link #create(String)} should always contain a full path
	 *                 to a desired font file. May be {@code null} to indicate that the default path
	 *                 should be used.
	 */
	public static void setBasePath(@Nullable final String basePath) {
		Font.basePath = basePath == null ? FONT_DIR : basePath;
	}

	/**
	 * Creates a new instance of Font with the font path obtained from the given <var>attrs</var> set.
	 *
	 * @param context      Context used to process the given attributes set.
	 * @param attrs        Attributes set which that is expected to have {@link R.attr#uiFont uiFont}
	 *                     attribute specified with font path.
	 * @param defStyleAttr An attribute of the default style presented within the current theme which
	 *                     supplies default attributes for {@link TypedArray}.
	 * @param defStyleRes  Resource id of the default style used when the <var>defStyleAttr</var> is 0
	 *                     or the current theme does not have style for <var>defStyleAttr</var> specified.
	 * @return New or cached instance of Font or {@code null} if font path parsed from the given attributes
	 * set is invalid.
	 *
	 * @see #create(String)
	 * @see #create(Context, int)
	 */
	@Nullable public static Font create(
			@NonNull final Context context,
			@Nullable final AttributeSet attrs,
			@AttrRes final int defStyleAttr,
			@StyleRes final int defStyleRes
	) {
		final Resources.Theme theme = context.getTheme();
		final TypedArray attributes = theme.obtainStyledAttributes(attrs, new int[]{R.attr.uiFont}, defStyleAttr, defStyleRes);
		final String filePath = attributes.getString(0);
		attributes.recycle();
		return TextUtils.isEmpty(filePath) ? null : create(filePath);
	}

	/**
	 * Creates a new instance of Font with the font path obtained from the given <var>style</var>.
	 *
	 * @param context Context used to process the given style.
	 * @param style   Resource id of the desired style that is expected to have {@link R.attr#uiFont uiFont}
	 *                attribute specified with font path.
	 * @return New or cached instance of Font or {@code null} if font path parsed from the given style
	 * is invalid.
	 *
	 * @see #create(String)
	 * @see #create(Context, AttributeSet, int, int)
	 */
	@Nullable public static Font create(@NonNull final Context context, @StyleRes final int style) {
		final Resources.Theme theme = context.getTheme();
		final TypedArray attributes = theme.obtainStyledAttributes(style, new int[]{R.attr.uiFont});
		final String filePath = attributes.getString(0);
		attributes.recycle();
		return TextUtils.isEmpty(filePath) ? null : create(filePath);
	}

	/**
	 * Creates an instance of Font with the given file path.
	 *
	 * @return New or cached instance of Font for the specified path.
	 * @throws IllegalArgumentException If the given <var>filePath</var> is empty.
	 *
	 * @see #create(Context, AttributeSet, int, int)
	 * @see #create(Context, int)
	 * @see #Font(String)
	 */
	@NonNull public static Font create(@NonNull final String filePath) {
		if (TextUtils.isEmpty(filePath)) {
			throw new IllegalArgumentException("Font path cannot be empty.");
		}
		final String fullFilePath = buildFullPath(filePath);
		if (cache.containsKey(fullFilePath)) {
			logDebugMessage("Re-using cached font for path(" + fullFilePath + ").");
			return cache.get(fullFilePath);
		}
		logDebugMessage("Creating and caching new font for path(" + fullFilePath + ").");
		final Font font = new Font(fullFilePath);
		cache.put(fullFilePath, font);
		return font;
	}

	/**
	 * Builds a full path to the .ttf font file from the given <var>filePath</var>.
	 *
	 * @param filePath The path that should be build as full path to the .ttf file within assets.
	 * @return Full path containing the specified file path with added {@link #TTF_SUFFIX} if not
	 * presented in the specified path and also prepended with {@link #basePath}.
	 */
	private static String buildFullPath(final String filePath) {
		return basePath + (TTF_SUFFIX_MATCHER.reset(filePath).matches() ? filePath : filePath + TTF_SUFFIX);
	}

	/**
	 * Clears the current cache with already created and cached Font instances.
	 * <p>
	 * <b>Note that this will not invalidate those instances as they may be in use across the application.</b>
	 */
	public static void clearCache() {
		cache.clear();
	}

	/**
	 * Returns the path to the font file that is represented by this font instance.
	 *
	 * @return Full path to the .ttf file within an application assets folder.
	 *
	 * @see #create(String)
	 * @see #setBasePath(String)
	 */
	@NonNull public String getFilePath() {
		return filePath;
	}

	/**
	 * Returns instance of the Typeface which is created for the .ttf file of this font instance.
	 * <p>
	 * Subsequent calls to this method will return already created and cached Typeface object.
	 *
	 * @param context Valid context used to create requested TypeFace.
	 * @return Typeface associated with this font instance.
	 * @throws IllegalArgumentException If font file at the path specified for this
	 *                                  font does not exist or it is not a valid .ttf file.
	 *
	 * @see Typeface#createFromAsset(android.content.res.AssetManager, String)
	 */
	@NonNull public Typeface getTypeface(@NonNull final Context context) {
		if (typeFace == null) {
			try {
				this.typeFace = Typeface.createFromAsset(context.getAssets(), filePath);
			} catch (Exception e) {
				throw new IllegalArgumentException(
						"Cannot create Typeface for font at path(" + filePath + "). " +
								"Font file at the specified path does not exist or it is not a valid .ttf file.",
						e
				);
			}
		}
		return typeFace;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}