/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Interface for widgets which support usage of custom {@link Font} that may be specified via
 * {@link #setFont(Font)}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public interface FontWidget {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Defines an annotation for determining available typeface styles that may be passed to
	 * {@link #setTypeface(Typeface, int)} method.
	 */
	@IntDef({
			Typeface.NORMAL,
			Typeface.BOLD,
			Typeface.ITALIC,
			Typeface.BOLD_ITALIC
	})
	@Retention(RetentionPolicy.SOURCE)
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public @interface TypefaceStyle {}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the context in which is this font widget created.
	 *
	 * @return The associated context.
	 */
	@NonNull Context getContext();

	/**
	 * Sets a path to the font file of which font should be applied to this font widget.
	 *
	 * @param fontPath Path to the font file from which to create the desired font and apply it to
	 *                 this font widget.
	 *
	 * @see R.attr#uiFont ui:uiFont
	 * @see Font#create(String)
	 */
	void setFont(@NonNull String fontPath);

	/**
	 * Sets a font to be applied to this font widget.
	 *
	 * @param font The desired font to be applied to this font widget.
	 *
	 * @see #setFont(String)
	 * @see Font#getTypeface(Context)
	 */
	void setFont(@NonNull Font font);

	/**
	 * Sets a typeface in which the text of this font widget should be displayed.
	 * <p>
	 * <b>Note</b>, that not all Typeface families actually have bold and italic variants, so you may
	 * need to use {@link #setTypeface(Typeface, int)} to get the appearance that you actually want.
	 *
	 * @param typeface The desired typeface. May be {@code null} to use a default one.
	 */
	void setTypeface(@Nullable Typeface typeface);

	/**
	 * Sets a typeface and style in which the text of this font widget should be displayed and turns
	 * on the fake bold and italic bits in the Paint if the Typeface that you provided does not have
	 * all the bits in the style that you specified.
	 *
	 * @param typeface The desired typeface. May be {@code null} to create default one according to
	 *                 the specified <var>style</var>.
	 * @param style    One of styles defined by {@link TypefaceStyle @TypefaceStyle} annotation.
	 *
	 * @see #setTypeface(Typeface)
	 */
	void setTypeface(@Nullable Typeface typeface, @TypefaceStyle int style);
}