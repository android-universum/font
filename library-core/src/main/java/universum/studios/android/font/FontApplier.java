/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;

/**
 * Interface for simple mediator that may be used in order to apply a desired custom font to a
 * specific {@link FontWidget} implementation.
 *
 * @author Martin Albedinsky
 * @since 1.1
 */
public interface FontApplier {

	/*
	 * Implementations =============================================================================
	 */

	/**
	 * Default implementation of {@link FontApplier} used by all font widgets.
	 */
	FontApplier DEFAULT = new FontApplier() {

		/**
		 */
		@Override public boolean applyFont(
				@NonNull final FontWidget widget,
				@Nullable final AttributeSet attrs,
				@AttrRes final int defStyleAttr,
				@StyleRes final int defStyleRes
		) {
			return applyFont(widget, Font.create(widget.getContext(), attrs, defStyleAttr, defStyleRes));
		}

		/**
		 */
		@Override public boolean applyFont(@NonNull final FontWidget widget, @StyleRes final int style) {
			return applyFont(widget, Font.create(widget.getContext(), style));
		}

		/**
		 */
		@Override public boolean applyFont(@NonNull final FontWidget widget, @NonNull final String fontPath) {
			return applyFont(widget, Font.create(fontPath));
		}

		/**
		 */
		@Override public boolean applyFont(@NonNull final FontWidget widget, @Nullable final Font font) {
			if (font == null) {
				return false;
			}
			widget.setTypeface(font.getTypeface(widget.getContext()));
			return true;
		}
	};

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Applies a {@link Font} created for the specified <var>attrs</var> set to the given font
	 * <var>widget</var>.
	 *
	 * @param widget       The widget to which to apply the desired font.
	 * @param attrs        Attributes set which contains {@link R.attr#uiFont uiFont} attribute with path
	 *                     to the .ttf file for which to create the desired font.
	 * @param defStyleAttr Attribute which contains a reference to a default style resource for the
	 *                     given widget within a theme of the widget's associated context.
	 * @param defStyleRes  Resource id of the default style for the widget.
	 * @return {@code True} if the desired font has been applied, {@code false} otherwise.
	 *
	 * @see Font#create(Context, AttributeSet, int, int)
	 * @see #applyFont(FontWidget, Font)
	 */
	boolean applyFont(@NonNull final FontWidget widget, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes);

	/**
	 * Applies a {@link Font} created for the specified <var>style</var> to the given font <var>widget</var>.
	 *
	 * @param widget The widget to which to apply the desired font.
	 * @param style  Resource to style which contains {@link R.attr#uiFont uiFont} attribute with
	 *               path to the .ttf file for which to create the desired font.
	 * @return {@code True} if the desired font has been applied, {@code false} otherwise.
	 * @see Font#create(Context, int)
	 * @see #applyFont(FontWidget, Font)
	 */
	boolean applyFont(@NonNull final FontWidget widget, @StyleRes final int style);

	/**
	 * Applies a {@link Font} created for the specified <var>fontPath</var> to the given font <var>widget</var>.
	 *
	 * @param widget   The widget to which to apply the desired font.
	 * @param fontPath Path to the .ttf file for which to create the desired font.
	 * @return {@code True} if the desired font has been applied, {@code false} otherwise.
	 *
	 * @see Font#create(String)
	 * @see #applyFont(FontWidget, Font)
	 */
	boolean applyFont(@NonNull final FontWidget widget, @NonNull final String fontPath);

	/**
	 * Applies the specified <var>font</var> to the given font <var>widget</var>.
	 *
	 * @param widget The widget to which to apply the desired font.
	 * @param font   The desired font to apply to the given widget.
	 * @return {@code True} if the desired font has been applied, {@code false} otherwise.
	 *
	 * @see Font#getTypeface(Context)
	 */
	boolean applyFont(@NonNull final FontWidget widget, @Nullable final Font font);
}