/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import androidx.annotation.NonNull;

/**
 * A {@link FontWidget} extended interface that allows to specify a custom {@link FontApplier} that
 * should be used by font widget implementation to perform custom font applying logic.
 *
 * @author Martin Albedinsky
 * @since 1.1
 */
public interface FontApplierWidget extends FontWidget {

	/**
	 * Sets an applier that should be used by this font widget in order to apply custom font requested
	 * via {@link #setFont(Font)}.
	 *
	 * @param applier The desired applier.
	 */
	void setFontApplier(@NonNull FontApplier applier);
}