/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font;

import android.util.Log;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

/**
 * @author Martin Albedinsky
 */
public final class FontTest extends AndroidTestCase {

	private static final String FONT_PATH = "redressed.ttf";

	@Override public void afterTest() {
		super.afterTest();
		// Ensure that the Font class is always in the initial state.
		Font.setBasePath(null);
		Font.clearCache();
	}

	@Test public void testSetLogLevel() {
		// Act:
		Font.setLogLevel(Log.DEBUG);
	}

	@Test public void testSetBasePath() {
		// Act + Assert:
		Font.setBasePath("");
		assertThat(Font.create(FONT_PATH).getFilePath(), is(FONT_PATH));
		Font.setBasePath("fonts" + File.separator);
		assertThat(Font.create(FONT_PATH).getFilePath(), is("fonts" + File.separator + FONT_PATH));
		Font.setBasePath(null);
		assertThat(Font.create(FONT_PATH).getFilePath(), is("font" + File.separator + FONT_PATH));
	}

	@Test public void testInstantiation() {
		// Act:
		final Font font = new Font(FONT_PATH);
		// Assert:
		assertThat(font.getFilePath(), is(FONT_PATH));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInstantiationWithEmptyPath() {
		// Act:
		new Font("");
	}

	@Ignore("We cannot mock final classes from the Android framework.")
	@Test public void testCreateFromAttributeSetWithPath() {
		/*final Context mockContext = mock(Context.class);
		final Resources.Theme mockTheme = mock(Resources.Theme.class);
		final AttributeSet mockAttributeSet = mock(AttributeSet.class);
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockContext.getTheme()).thenReturn(mockTheme);
		when(mockTheme.obtainStyledAttributes(mockAttributeSet, any(int[].class), 0, 0)).thenReturn(mockTypedArray);
		when(mockTypedArray.getString(0)).thenReturn(FONT_PATH);
		final Font font = Font.create(mockContext, mockAttributeSet, 0, 0);
		assertThat(font, is(not(nullValue())));
		assertThat(font.getFilePath(), is(Font.FONT_DIR + FONT_PATH));*/
	}

	@Ignore("We cannot mock final classes from the Android framework.")
	@Test public void testCreateFromAttributeSetWithoutPath() {
		/*final Context mockContext = mock(Context.class);
		final Resources.Theme mockTheme = mock(Resources.Theme.class);
		final AttributeSet mockAttributeSet = mock(AttributeSet.class);
		final TypedArray mockTypedArray = mock(TypedArray.class);
		when(mockContext.getTheme()).thenReturn(mockTheme);
		when(mockTheme.obtainStyledAttributes(mockAttributeSet, any(int[].class), 0, 0)).thenReturn(mockTypedArray);
		when(mockTypedArray.getString(0)).thenReturn("");
		assertThat(Font.create(mockContext, mockAttributeSet, 0, 0), is(nullValue()));*/
	}

	@Test public void testCreateFromPath() {
		// Act:
		final Font font = Font.create(FONT_PATH);
		// Assert:
		assertThat(font, is(not(nullValue())));
		assertThat(font.getFilePath(), is(Font.FONT_DIR + FONT_PATH));
		assertThat(font, is(Font.create(FONT_PATH)));
	}

	@Test public void testCreateFromPathWithoutTTFSuffix() {
		// Arrange:
		final String path = FONT_PATH.substring(0, FONT_PATH.indexOf("."));
		// Act:
		final Font font = Font.create(path);
		// Assert:
		assertThat(font, is(notNullValue()));
		assertThat(font.getFilePath(), is(Font.FONT_DIR + FONT_PATH));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreateFromEmptyPath() {
		// Act:
		Font.create("");
	}

	@Test public void testClearCache() {
		// Arrange:
		final Font font = Font.create(FONT_PATH);
		// Act:
		Font.clearCache();
		// Assert:
		assertThat(font, is(not(Font.create(FONT_PATH))));
	}

	@Test(expected = RuntimeException.class)
	public void testGetTypefaceForNotExistingPath() {
		// Arrange:
		final Font font = new Font("not_existing_font.ttf");
		// Act:
		font.getTypeface(context());
	}
}