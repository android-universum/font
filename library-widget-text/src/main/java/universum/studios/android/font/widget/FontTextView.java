/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.font.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.font.Font;
import universum.studios.android.font.FontApplier;
import universum.studios.android.font.FontApplierWidget;

/**
 * A {@link TextView} and {@link FontApplierWidget} implementation that supports applying of custom
 * font via {@link #setFont(Font)}.
 * <p>
 * This font widget implementation also allows to specify custom font via
 * {@link universum.studios.android.font.R.attr#uiFont uiFont} attribute through Xml {@link AttributeSet}
 * or through text appearance style which may be set via {@link #setTextAppearance(int)}.
 *
 * @author Martin Albedinsky
 * @since 1.o
 */
public class FontTextView extends TextView implements FontApplierWidget {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "FontTextView";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Applier to which is this font widget delegating applying of {@link Font} whenever one of
	 * {@link #setFont(String)}, {@link #setFont(Font)} or {@link #setTextAppearance(Context, int)}
	 * methods is invoked.
	 */
	private FontApplier fontApplier = FontApplier.DEFAULT;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #FontTextView(Context, AttributeSet)} without attributes.
	 */
	public FontTextView(@NonNull final Context context) {
		this(context, null);
	}

	/**
	 * Same as {@link #FontTextView(Context, AttributeSet, int)} with {@link android.R.attr#textViewStyle}
	 * as attribute for default style.
	 */
	public FontTextView(@NonNull final Context context, @Nullable final AttributeSet attrs) {
		this(context, attrs, android.R.attr.textViewStyle);
	}

	/**
	 * Same as {@link #FontTextView(Context, AttributeSet, int, int)} with {@code 0} as default style.
	 */
	public FontTextView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		if (!isInEditMode()) this.fontApplier.applyFont(this, attrs, defStyleAttr, 0);
	}

	/**
	 * Creates a new instance of FontTextView for the given <var>context</var>.
	 *
	 * @param context      Context in which will be the new view presented.
	 * @param attrs        Set of Xml attributes used to configure the new instance of view.
	 * @param defStyleAttr Attribute which contains a reference to a default style resource for view
	 *                     within a theme of the given context.
	 * @param defStyleRes  Resource id of the default style for the view.
	 */
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public FontTextView(@NonNull final Context context, @Nullable final AttributeSet attrs, @AttrRes final int defStyleAttr, @StyleRes final int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		if (!isInEditMode()) this.fontApplier.applyFont(this, attrs, defStyleAttr, defStyleRes);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public void setFontApplier(@NonNull final FontApplier applier) {
		this.fontApplier = applier;
	}

	/**
	 */
	@Override public void setFont(@NonNull final String fontPath) {
		this.fontApplier.applyFont(this, fontPath);
	}

	/**
	 */
	@Override public void setFont(@NonNull final Font font) {
		this.fontApplier.applyFont(this, font);
	}

	/**
	 */
	@SuppressWarnings("deprecation")
	@Override public void setTextAppearance(@NonNull final Context context, @StyleRes final int resId) {
		super.setTextAppearance(context, resId);
		this.fontApplier.applyFont(this, resId);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}