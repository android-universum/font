Font-Widget-Core
===============

This module contains core components used by`FontWidget` implementations.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Afont/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Afont/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:font-widget-core:${DESIRED_VERSION}@aar"

_depends on:_
[font-core](https://bitbucket.org/android-universum/font/src/main/library-core)