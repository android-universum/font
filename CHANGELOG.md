Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.2.2](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 20.03.2020

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.2.1](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.2.0](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 17.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [1.1.4](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 16.06.2018

- Small updates.

### [1.1.3](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 13.03.2018

- Removed setting of class name for **accessibility events** for all font widgets.

### [1.1.2](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 27.07.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_.

### [1.1.1](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 29.04.2017

- Removed **deprecated** components.

### [1.1.0](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 18.04.2017

- `FontConfig` class has been deprecated. The library related configuration may be performed via
  `Font` class.
- Library module **font-widget-text** has been split into **font-widget-text** and **font-widget-edit**
  modules.

### [1.0.0](https://bitbucket.org/android-universum/font/wiki/version/1.x) ###
> 18.01.2017

- First production release.